#pragma once
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <cstdlib>
#include <vector>
#include <sstream>

typedef struct {
	std::string tipo;
	int taglia;
	int quantita;
	float prezzo;
}item;


class vestiti{
private:
	int count;
	item it;
	std::vector<item> negozio;

	void azzeraVector(std::vector<item>*);
	int scriviPrivate(std::string);
	int leggiPrivate(std::string, std::vector<item>*);
	int quantPrivate(int, std::vector<item>);
	float prezzoPrivate(int, std::vector<item>);
	std::string tipiPrivate(int, std::vector<item>);
	
public:
	vestiti();
	vestiti(std::string);
	int scrivi(std::string);
	int leggi(std::string);
};

