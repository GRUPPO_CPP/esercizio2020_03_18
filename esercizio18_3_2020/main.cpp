#include <iostream>
#include "funzioni.h"

using namespace std;

int main()
{
    vestiti vs;
    //vestiti vs("VESTITI.TXT");

    try {
        vs.leggi("VESTITI.TXT");
    }
    catch (const char* msg) {
        cerr << msg << endl;
    }
    catch (int x) {
        cout << x;
        try {
            vs.scrivi("TOTALI.TXT");
        }
        catch (const char* msg) {
            cerr << msg << endl;
        }
    }

    return 0;
}
