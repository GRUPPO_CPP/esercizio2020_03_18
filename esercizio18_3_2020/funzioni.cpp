#include "funzioni.h"

using namespace std;

vestiti::vestiti() {
	this->count = 0;
}

vestiti::vestiti(string st) {
	this->count = 0;

	if (leggiPrivate(st, &(this->negozio))) {
		cout << "Letto i dati da => " << st << endl;
	}
	else {
		cout << "non esiste il file => " << st << endl;
	}
}

int vestiti::scrivi(string st) {
	return scriviPrivate(st);
}

int vestiti::scriviPrivate(string st) {
	int taglie[6] = {42, 44, 46, 48, 50, 52};
	int j;

	if (this->count < 20) {
		ofstream fp(st);	//ios::app

		if (fp.is_open()) {
			for(int i = 0; i < 6; i++)
				fp << taglie[i] << " " << this->quantPrivate(taglie[i], this->negozio) << " " << this->prezzoPrivate(taglie[i], this->negozio) << " " << this->tipiPrivate(taglie[i], this->negozio) << "\n";
			fp.close();
		}
		cout << "\ni dati sono salvati\n\n";
		return 1;
	}	
	else {
		throw "Impossibile scrivere il file TOTALI.TXT";
	}
}

int vestiti::leggi(std::string st) {
	return leggiPrivate(st, &(this->negozio));
}

int vestiti::leggiPrivate(string st, vector<item>* neg) {
	string tipo;
	int taglia, quantita;
	float prezzo;
	ifstream ReadFile;
	ReadFile.open(st);

	this->azzeraVector(neg);
	this->count = 0;

	if (ReadFile.is_open()) {
		while (!ReadFile.eof()) {
			ReadFile >> tipo;
			ReadFile >> taglia;
			ReadFile >> quantita;
			ReadFile >> prezzo;			

			this->it.tipo = tipo;
			this->it.taglia = taglia;
			this->it.quantita = quantita;
			this->it.prezzo = prezzo;
			
			if (!ReadFile.eof()) {
				neg->push_back(this->it);
				this->count++;
			}
		}

		return 1;
	}
	else {
		throw "Impossibile leggere il file VESTITI.TXT";
	}
}

void vestiti::azzeraVector(vector<item>* neg) {
	while (!neg->empty()) {
		neg->pop_back();
	}
}

int vestiti::quantPrivate(int taglia, vector<item> ng) {
	int tot = 0;

	for (auto i = ng.begin(); i != ng.end(); ++i) {
		if (i->taglia == taglia) {
			tot += i->quantita;
		 }		
	}
	return tot;
}

float vestiti::prezzoPrivate(int taglia, vector<item> ng) {
	float tot = 0;
	item it;

	for (auto i = ng.begin(); i != ng.end(); ++i) {
		if (i->taglia == taglia) {
			tot += i->prezzo;
		}
	}

	return tot;
}

string vestiti::tipiPrivate(int taglia, vector<item> ng) {
	string str = "";
	string temp;

	for (auto i = ng.begin(); i != ng.end(); ++i) {
		if (i->taglia == taglia) {	
			stringstream s;
			s << i->quantita;
			str += i->tipo;
			str += "("; 
			str += s.str();
			str += ") ";
		}
	}
	return str;
}